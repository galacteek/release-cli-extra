FROM registry.gitlab.com/gitlab-org/release-cli:latest

RUN apk add bash grep git curl wget python3 py3-pip
RUN pip3 install keepachangelog
RUN ln -sf /bin/bash /bin/sh

ENTRYPOINT []
